﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AXIOM_WIZARD{
	
	public class MeshMaker{

		List<Vector3> resultVertices = new List<Vector3>();
		List<Vector3> resultNormals = new List<Vector3>();
		List<Vector2> resultUVS = new List<Vector2>();
		List<Vector4> resultTg = new List<Vector4>();
		List<List<int>> resultSubIndices = new List<List<int>>();
        
	    public int VertCount
        {
            get
            {
                return resultVertices.Count;
            }
        }

        public void Clear()
        {
            resultVertices.Clear();
            resultNormals.Clear();
            resultUVS.Clear();
            resultTg.Clear();
            resultSubIndices.Clear();
        }

        public void AddTriangle(Triangle triangle, int submesh)
        {
            AddTriangle(triangle.vertices, triangle.uvs, triangle.normals, triangle.tg, submesh);
        }

		public void AddTriangle(Vector3[] vertices, Vector2[] uvs, Vector3[] normals, int submesh = 0)
        {
            AddTriangle(vertices, uvs, normals, null, submesh);
		}

        public void AddTriangle(Vector3[] vertices, Vector2[] uvs, Vector3[] normals, Vector4[] tg, int submesh = 0)
        {
			int vertCount = resultVertices.Count;

			resultVertices.Add(vertices[0]);
			resultVertices.Add(vertices[1]);
			resultVertices.Add(vertices[2]);

			resultNormals.Add(normals[0]);
			resultNormals.Add(normals[1]);
			resultNormals.Add(normals[2]);

			resultUVS.Add(uvs[0]);
			resultUVS.Add(uvs[1]);
			resultUVS.Add(uvs[2]);

            if (tg != null)
            {
                resultTg.Add(tg[0]);
                resultTg.Add(tg[1]);
                resultTg.Add(tg[2]);
            }

			if(resultSubIndices.Count < submesh+1){
				for(int i=resultSubIndices.Count; i<submesh+1; i++){
					resultSubIndices.Add(new List<int>());
				}
			}

			resultSubIndices[submesh].Add(vertCount);
			resultSubIndices[submesh].Add(vertCount+1);
			resultSubIndices[submesh].Add(vertCount+2);

		}

		public void RemoveDoubles(){

			int dubCount = 0;

			Vector3 vertex = Vector3.zero;
			Vector3 normal = Vector3.zero;
			Vector2 uv = Vector2.zero;
			Vector4 tangent= Vector4.zero;

			int j=0; 
			while(j < VertCount){

				vertex = resultVertices[j];
				normal = resultNormals[j];
				uv     = resultUVS[j];
				for(int backward_j=j-1; backward_j>=0; backward_j--){

					if(vertex  == resultVertices[backward_j] &&
						normal == resultNormals[backward_j] && 
						uv     == resultUVS[backward_j])
					{
						dubCount++;
						DoubleFound(backward_j, j);
						j--;
						break;
					}
				}

				j++;

			}

			Debug.LogFormat("Doubles found {0}", dubCount);
				
		}
		private void DoubleFound(int first, int duplicate)
        {
			for(int h=0; h<resultSubIndices.Count; h++){
				for(int i=0; i<resultSubIndices[h].Count; i++){

					if(resultSubIndices[h][i] > duplicate)
						resultSubIndices[h][i]--;
					else if( resultSubIndices[h][i] == duplicate)
						resultSubIndices[h][i] = first;
				}
			}

			resultVertices.RemoveAt(duplicate);
			resultNormals.RemoveAt(duplicate);
			resultUVS.RemoveAt(duplicate);

			if(resultTg.Count > 0)
				resultTg.RemoveAt(duplicate);

		}

		public Mesh GetMesh(){
			
			Mesh shape = new Mesh();
			shape.name = "Generated Mesh";
			shape.SetVertices(resultVertices);
			shape.SetNormals(resultNormals);
			shape.SetUVs(0, resultUVS);
			shape.SetUVs(1, resultUVS);
            
			if(resultTg.Count > 1)
				shape.SetTangents(resultTg);

			shape.subMeshCount = resultSubIndices.Count;

			for(int i=0; i<resultSubIndices.Count; i++)
				shape.SetTriangles(resultSubIndices[i], i);

			return shape;
		}

		#if UNITY_EDITOR
		
		public Mesh GetMesh_GenerateSecondaryUVSet(){

			Mesh shape = GetMesh();
		
			UnityEditor.Unwrapping.GenerateSecondaryUVSet(shape);

			return shape;
		}

		public Mesh GetMesh_GenerateSecondaryUVSet( UnityEditor.UnwrapParam param){

			Mesh shape = GetMesh();
			UnityEditor.Unwrapping.GenerateSecondaryUVSet(shape, param);

			return shape;
		}
		#endif

        public struct Triangle
        {
            public Vector3[] vertices;
            public Vector2[] uvs;
            public Vector3[] normals;
            public Vector4[] tg;

            public Triangle(Vector3[] vertices = null, Vector2[] uvs = null, Vector3[] normals = null, Vector4[] tg = null)
            {               
                this.vertices = vertices;
                this.uvs = uvs;      
                this.normals = normals;
                this.tg = tg;
            }
        }
        
	}
}