﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace AXIOM_WIZARD
{
    public class MeshCombiner : MonoBehaviour
    {
        Vector3 res = new Vector3(0, 0, 0);
        Vector3 ang = new Vector3(0, 0, 0);
        public Text tx;
        public void Combine(Material mat, GameObject[] objects)
        {
            for (int j = 0; j < objects.Length; j++)
            {
                objects[j].transform.parent = this.transform;
            }
            MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
            CombineInstance[] combine = new CombineInstance[meshFilters.Length];
            int i = 0;
            while (i < meshFilters.Length)
            {
                combine[i].mesh = meshFilters[i].sharedMesh;
                combine[i].transform = meshFilters[i].transform.localToWorldMatrix;
                i++;
            }
            transform.GetComponent<MeshFilter>().mesh = new Mesh();
            transform.GetComponent<MeshFilter>().mesh.CombineMeshes(combine);
            this.GetComponent<MeshRenderer>().material = mat;
            GameObject obj = this.gameObject;
            obj.AddComponent<MeshCollider>().convex = false;
            obj.GetComponent<MeshCollider>().convex = true;
            transform.gameObject.SetActive(true);
            transform.gameObject.GetComponent<MeshRenderer>().enabled = false;
            int count = obj.transform.childCount;
            for (int j = 0; j < count; j++)
            {
                res += obj.transform.GetChild(j).transform.position;
                ang += obj.transform.GetChild(j).transform.rotation.eulerAngles;
            }
            res /= count;
            Compare compare = new Compare();
            float r = compare.Comp(GameObject.Find("Wireframe"), res, ang);
          
            GameObject.Find("ResultText").GetComponent<Text>().text = "Точность детали: " + r.ToString() + "%";
        }
        public void Slice(GameObject obj)
        {
            int count = obj.transform.childCount;
            for (int i = 0; i < count; i++)
            {
                obj.transform.GetChild(0).gameObject.SetActive(true);
                obj.transform.GetChild(0).parent = null;
            }
            Destroy(obj);
        }
    }
}