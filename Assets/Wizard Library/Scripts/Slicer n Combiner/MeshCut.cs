﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace AXIOM_WIZARD{

	public class MeshCut{
        
		private static Plane _blade;
		private static Mesh  _victim_mesh;

        private static MeshMaker _leftSide = new MeshMaker();
		private static MeshMaker _rightSide = new MeshMaker();
        private static MeshMaker.Triangle _triangleCache = new MeshMaker.Triangle( new Vector3[3], new Vector2[3], new Vector3[3], new Vector4[3] );
        private static List<Vector3>       _newVerticesCache = new List<Vector3>();
        private static bool[]              _isLeftSideCache = new bool[3];
        private static int                 _capMatSub = 1;
       

        public static GameObject[] Cut(GameObject victim, Vector3 anchorPoint, Vector3 normalDirection, Material capMaterial){
           
			_blade = new Plane(victim.transform.InverseTransformDirection(-normalDirection),
				victim.transform.InverseTransformPoint(anchorPoint));

			_victim_mesh = victim.GetComponent<MeshFilter>().mesh;

            _leftSide.Clear();
            _rightSide.Clear();
            _newVerticesCache.Clear();

            
			int   index_1, index_2, index_3;

            var mesh_vertices = _victim_mesh.vertices;
            var mesh_normals  = _victim_mesh.normals;
            var mesh_uvs      = _victim_mesh.uv;
            var mesh_tg = _victim_mesh.tangents;
            if (mesh_tg != null && mesh_tg.Length == 0)
                mesh_tg = null;

			for (int submeshIterator=0; submeshIterator<_victim_mesh.subMeshCount; submeshIterator++){

				var indices = _victim_mesh.GetTriangles(submeshIterator);
                
				for(int i=0; i<indices.Length; i+=3){

					index_1 = indices[i];
					index_2 = indices[i+1];
					index_3 = indices[i+2];

                    _triangleCache.vertices[0] = mesh_vertices[index_1];
                    _triangleCache.vertices[1] = mesh_vertices[index_2];
                    _triangleCache.vertices[2] = mesh_vertices[index_3];

                    _triangleCache.normals[0] = mesh_normals[index_1];
                    _triangleCache.normals[1] = mesh_normals[index_2];
                    _triangleCache.normals[2] = mesh_normals[index_3];

                    _triangleCache.uvs[0] = mesh_uvs[index_1];
                    _triangleCache.uvs[1] = mesh_uvs[index_2];
                    _triangleCache.uvs[2] = mesh_uvs[index_3];

                    if (mesh_tg != null)
                    {
                        _triangleCache.tg[0] = mesh_tg[index_1];
                        _triangleCache.tg[1] = mesh_tg[index_2];
                        _triangleCache.tg[2] = mesh_tg[index_3];
                    }
                    else
                    {
                        _triangleCache.tg[0] = Vector4.zero;
                        _triangleCache.tg[1] = Vector4.zero;
                        _triangleCache.tg[2] = Vector4.zero;
                    }

                    _isLeftSideCache[0] = _blade.GetSide(mesh_vertices[index_1]);
					_isLeftSideCache[1] = _blade.GetSide(mesh_vertices[index_2]);
					_isLeftSideCache[2] = _blade.GetSide(mesh_vertices[index_3]);
                    

					if(_isLeftSideCache[0] == _isLeftSideCache[1] && _isLeftSideCache[0] == _isLeftSideCache[2]){

						if(_isLeftSideCache[0])
							_leftSide.AddTriangle( _triangleCache, submeshIterator);
						else 
							_rightSide.AddTriangle(_triangleCache, submeshIterator);

					}else{ 
						
						Cut_this_Face(ref _triangleCache, submeshIterator);
					}
				}
			}

			Material[] mats = victim.GetComponent<MeshRenderer>().sharedMaterials;
			if(mats[mats.Length-1].name != capMaterial.name){
				Material[] newMats = new Material[mats.Length+1];
				mats.CopyTo(newMats, 0);
				newMats[mats.Length] = capMaterial;
				mats = newMats;
			}
			_capMatSub = mats.Length-1;

			Cap_the_Cut();


			Mesh left_HalfMesh = _leftSide.GetMesh();
			left_HalfMesh.name =  "Split Mesh Left";

			Mesh right_HalfMesh = _rightSide.GetMesh();
			right_HalfMesh.name = "Split Mesh Right";

			victim.name = "left side";
			victim.GetComponent<MeshFilter>().mesh = left_HalfMesh;

			GameObject rightSideObj = victim;
			GameObject leftSideObj = new GameObject("right side", typeof(MeshFilter), typeof(MeshRenderer));
			leftSideObj.transform.position = victim.transform.position;
			leftSideObj.transform.rotation = victim.transform.rotation;
			leftSideObj.GetComponent<MeshFilter>().mesh = right_HalfMesh;
		
			if(victim.transform.parent != null){
				leftSideObj.transform.parent = victim.transform.parent;
			}

			leftSideObj.transform.localScale = victim.transform.localScale;

			rightSideObj.GetComponent<MeshRenderer>().materials = mats;
			leftSideObj.GetComponent<MeshRenderer>().materials = mats;

			return new GameObject[]{ rightSideObj, leftSideObj };

		}

        #region Cutting
        private static MeshMaker.Triangle _leftTriangleCache  = new MeshMaker.Triangle(new Vector3[3], new Vector2[3], new Vector3[3], new Vector4[3]);
        private static MeshMaker.Triangle _rightTriangleCache = new MeshMaker.Triangle(new Vector3[3], new Vector2[3], new Vector3[3], new Vector4[3]);
        private static MeshMaker.Triangle _newTriangleCache  = new MeshMaker.Triangle(new Vector3[3], new Vector2[3], new Vector3[3], new Vector4[3]);

        private static void Cut_this_Face(ref MeshMaker.Triangle triangle, int submesh)
        {

            _isLeftSideCache[0] = _blade.GetSide(triangle.vertices[0]);
            _isLeftSideCache[1] = _blade.GetSide(triangle.vertices[1]);
            _isLeftSideCache[2] = _blade.GetSide(triangle.vertices[2]);


            int leftCount = 0;
            int rightCount = 0;

            for (int i = 0; i < 3; i++)
            {
                if (_isLeftSideCache[i])
                {

                    _leftTriangleCache.vertices[leftCount] = triangle.vertices[i];
                    _leftTriangleCache.uvs[leftCount] = triangle.uvs[i];
                    _leftTriangleCache.normals[leftCount] = triangle.normals[i];
                    _leftTriangleCache.tg[leftCount] = triangle.tg[i];
                    
                    leftCount++;
                }
                else
                {

                    _rightTriangleCache.vertices[rightCount] = triangle.vertices[i];
                    _rightTriangleCache.uvs[rightCount] = triangle.uvs[i];
                    _rightTriangleCache.normals[rightCount] = triangle.normals[i];
                    _rightTriangleCache.tg[rightCount] = triangle.tg[i];

                    rightCount++;
                }
            }

            if (leftCount == 1)
            {
                _triangleCache.vertices[0] = _leftTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _leftTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _leftTriangleCache.normals[0];
                _triangleCache.tg[0] = _leftTriangleCache.tg[0];

                _triangleCache.vertices[1] = _rightTriangleCache.vertices[0];
                _triangleCache.uvs[1]      = _rightTriangleCache.uvs[0];
                _triangleCache.normals[1]  = _rightTriangleCache.normals[0];
                _triangleCache.tg[1] = _rightTriangleCache.tg[0];
                                                   
                _triangleCache.vertices[2] = _rightTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _rightTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _rightTriangleCache.normals[1];
                _triangleCache.tg[2] = _rightTriangleCache.tg[1];
            }
            else
            {
                _triangleCache.vertices[0] = _rightTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _rightTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _rightTriangleCache.normals[0];
                _triangleCache.tg[0] = _rightTriangleCache.tg[0];

                _triangleCache.vertices[1] = _leftTriangleCache.vertices[0];
                _triangleCache.uvs[1]      = _leftTriangleCache.uvs[0];
                _triangleCache.normals[1]  = _leftTriangleCache.normals[0];
                _triangleCache.tg[1] = _leftTriangleCache.tg[0];
                                                   
                _triangleCache.vertices[2] = _leftTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _leftTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _leftTriangleCache.normals[1];
                _triangleCache.tg[2] = _leftTriangleCache.tg[1];
            }


            float distance = 0;
            float normalizedDistance = 0.0f;
            Vector3 edgeVector = Vector3.zero;

            edgeVector = _triangleCache.vertices[1] - _triangleCache.vertices[0];
            _blade.Raycast(new Ray(_triangleCache.vertices[0], edgeVector.normalized), out distance);

            normalizedDistance = distance / edgeVector.magnitude;
            _newTriangleCache.vertices[0] = Vector3.Lerp(_triangleCache.vertices[0], _triangleCache.vertices[1], normalizedDistance);
            _newTriangleCache.uvs[0]      = Vector2.Lerp(_triangleCache.uvs[0],      _triangleCache.uvs[1],      normalizedDistance);
            _newTriangleCache.normals[0]  = Vector3.Lerp(_triangleCache.normals[0],  _triangleCache.normals[1],  normalizedDistance);
            _newTriangleCache.tg[0] = Vector4.Lerp(_triangleCache.tg[0], _triangleCache.tg[1], normalizedDistance);

            edgeVector = _triangleCache.vertices[2] - _triangleCache.vertices[0];
            _blade.Raycast(new Ray(_triangleCache.vertices[0], edgeVector.normalized), out distance);

            normalizedDistance = distance / edgeVector.magnitude;
            _newTriangleCache.vertices[1] = Vector3.Lerp(_triangleCache.vertices[0], _triangleCache.vertices[2], normalizedDistance);
            _newTriangleCache.uvs[1]      = Vector2.Lerp(_triangleCache.uvs[0],      _triangleCache.uvs[2],      normalizedDistance);
            _newTriangleCache.normals[1]  = Vector3.Lerp(_triangleCache.normals[0],  _triangleCache.normals[2],  normalizedDistance);
            _newTriangleCache.tg[1] = Vector4.Lerp(_triangleCache.tg[0], _triangleCache.tg[2], normalizedDistance);

            if (_newTriangleCache.vertices[0] != _newTriangleCache.vertices[1])
            {
                _newVerticesCache.Add(_newTriangleCache.vertices[0]);
                _newVerticesCache.Add(_newTriangleCache.vertices[1]);
            }

            if (leftCount == 1)
            {
                _triangleCache.vertices[0] = _leftTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _leftTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _leftTriangleCache.normals[0];
                _triangleCache.tg[0] = _leftTriangleCache.tg[0];

                _triangleCache.vertices[1] = _newTriangleCache.vertices[0];
                _triangleCache.uvs[1]      = _newTriangleCache.uvs[0];
                _triangleCache.normals[1]  = _newTriangleCache.normals[0];
                _triangleCache.tg[1] = _newTriangleCache.tg[0];
                                                   
                _triangleCache.vertices[2] = _newTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _newTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _newTriangleCache.normals[1];
                _triangleCache.tg[2] = _newTriangleCache.tg[1];
                
                NormalCheck(ref _triangleCache);

                _leftSide.AddTriangle(_triangleCache, submesh);

                _triangleCache.vertices[0] = _rightTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _rightTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _rightTriangleCache.normals[0];
                _triangleCache.tg[0] = _rightTriangleCache.tg[0];

                _triangleCache.vertices[1] = _newTriangleCache.vertices[0];
                _triangleCache.uvs[1]      = _newTriangleCache.uvs[0];
                _triangleCache.normals[1]  = _newTriangleCache.normals[0];
                _triangleCache.tg[1] = _newTriangleCache.tg[0];

                _triangleCache.vertices[2] = _newTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _newTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _newTriangleCache.normals[1];
                _triangleCache.tg[2] = _newTriangleCache.tg[1];
                
                NormalCheck(ref _triangleCache);

                _rightSide.AddTriangle(_triangleCache, submesh);

                _triangleCache.vertices[0] = _rightTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _rightTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _rightTriangleCache.normals[0];
                _triangleCache.tg[0] = _rightTriangleCache.tg[0];

                _triangleCache.vertices[1] = _rightTriangleCache.vertices[1];
                _triangleCache.uvs[1]      = _rightTriangleCache.uvs[1];
                _triangleCache.normals[1]  = _rightTriangleCache.normals[1];
                _triangleCache.tg[1] = _rightTriangleCache.tg[1];

                _triangleCache.vertices[2] = _newTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _newTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _newTriangleCache.normals[1];
                _triangleCache.tg[2] = _newTriangleCache.tg[1];

                NormalCheck(ref _triangleCache);

                _rightSide.AddTriangle(_triangleCache, submesh);
            }
            else
            {
                _triangleCache.vertices[0] = _rightTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _rightTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _rightTriangleCache.normals[0];
                _triangleCache.tg[0] = _rightTriangleCache.tg[0];

                _triangleCache.vertices[1] = _newTriangleCache.vertices[0];
                _triangleCache.uvs[1]      = _newTriangleCache.uvs[0];
                _triangleCache.normals[1]  = _newTriangleCache.normals[0];
                _triangleCache.tg[1] = _newTriangleCache.tg[0];

                _triangleCache.vertices[2] = _newTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _newTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _newTriangleCache.normals[1];
                _triangleCache.tg[2] = _newTriangleCache.tg[1];

                NormalCheck(ref _triangleCache);

                _rightSide.AddTriangle(_triangleCache, submesh);

                _triangleCache.vertices[0] = _leftTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _leftTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _leftTriangleCache.normals[0];
                _triangleCache.tg[0] = _leftTriangleCache.tg[0];

                _triangleCache.vertices[1] = _newTriangleCache.vertices[0];
                _triangleCache.uvs[1]      = _newTriangleCache.uvs[0];
                _triangleCache.normals[1]  = _newTriangleCache.normals[0];
                _triangleCache.tg[1] = _newTriangleCache.tg[0];

                _triangleCache.vertices[2] = _newTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _newTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _newTriangleCache.normals[1];
                _triangleCache.tg[2] = _newTriangleCache.tg[1];

                NormalCheck(ref _triangleCache);

                _leftSide.AddTriangle(_triangleCache, submesh);

                _triangleCache.vertices[0] = _leftTriangleCache.vertices[0];
                _triangleCache.uvs[0]      = _leftTriangleCache.uvs[0];
                _triangleCache.normals[0]  = _leftTriangleCache.normals[0];
                _triangleCache.tg[0] = _leftTriangleCache.tg[0];
                                                   
                _triangleCache.vertices[1] = _leftTriangleCache.vertices[1];
                _triangleCache.uvs[1]      = _leftTriangleCache.uvs[1];
                _triangleCache.normals[1]  = _leftTriangleCache.normals[1];
                _triangleCache.tg[1] = _leftTriangleCache.tg[1];

                _triangleCache.vertices[2] = _newTriangleCache.vertices[1];
                _triangleCache.uvs[2]      = _newTriangleCache.uvs[1];
                _triangleCache.normals[2]  = _newTriangleCache.normals[1];
                _triangleCache.tg[2] = _newTriangleCache.tg[1];

                NormalCheck(ref _triangleCache);

                _leftSide.AddTriangle(_triangleCache, submesh);
            }
            
        }
        #endregion

        #region Capping
        private static List<int> _capUsedIndicesCache = new List<int>();
		private static List<int> _capPolygonIndicesCache = new List<int>();

		private static void Cap_the_Cut(){

            _capUsedIndicesCache.Clear();
            _capPolygonIndicesCache.Clear();

            for (int i = 0; i < _newVerticesCache.Count; i+=2)
            {
                if (!_capUsedIndicesCache.Contains(i))
                {
                  
                    _capPolygonIndicesCache.Clear();
                    _capPolygonIndicesCache.Add(i);
                    _capPolygonIndicesCache.Add(i + 1);

                    _capUsedIndicesCache.Add(i);
                    _capUsedIndicesCache.Add(i + 1);

                    Vector3 connectionPointLeft  = _newVerticesCache[i];
                    Vector3 connectionPointRight = _newVerticesCache[i + 1];
                    bool isDone = false;

                   
                    while (!isDone)
                    {
                        isDone = true;

                        
                        for (int index = 0; index < _newVerticesCache.Count; index += 2)
                        {  
                            if (!_capUsedIndicesCache.Contains(index)) 
                            {
                                Vector3 nextPoint1 = _newVerticesCache[index];
                                Vector3 nextPoint2 = _newVerticesCache[index + 1];

                                
                                if (connectionPointLeft == nextPoint1 ||
                                    connectionPointLeft == nextPoint2 ||
                                    connectionPointRight == nextPoint1 ||
                                    connectionPointRight == nextPoint2)
                                {
                                    _capUsedIndicesCache.Add(index);
                                    _capUsedIndicesCache.Add(index + 1);

                                    
                                    if (connectionPointLeft == nextPoint1)
                                    {
                                        _capPolygonIndicesCache.Insert(0, index + 1);
                                        connectionPointLeft = _newVerticesCache[index + 1];
                                    }
                                    else if (connectionPointLeft == nextPoint2)
                                    {
                                        _capPolygonIndicesCache.Insert(0, index);
                                        connectionPointLeft = _newVerticesCache[index];
                                    }
                                    else if (connectionPointRight == nextPoint1)
                                    {
                                        _capPolygonIndicesCache.Add(index + 1);
                                        connectionPointRight = _newVerticesCache[index + 1];
                                    }
                                    else if (connectionPointRight == nextPoint2)
                                    {
                                        _capPolygonIndicesCache.Add(index);
                                        connectionPointRight = _newVerticesCache[index];
                                    }
                                    
                                    isDone = false;
                                }
                            }
                        }
                    }
                     if (_newVerticesCache[_capPolygonIndicesCache[0]] == _newVerticesCache[_capPolygonIndicesCache[_capPolygonIndicesCache.Count - 1]])
                        _capPolygonIndicesCache[_capPolygonIndicesCache.Count - 1] = _capPolygonIndicesCache[0];
                    else
                        _capPolygonIndicesCache.Add(_capPolygonIndicesCache[0]);

                    FillCap_Method1(_capPolygonIndicesCache);
                }
            }
		}
		private static void FillCap_Method1(List<int> indices){

            Vector3 center = Vector3.zero;
            foreach (var index in indices)
                center += _newVerticesCache[index];

            center = center / indices.Count;

            Vector3 upward = Vector3.zero;
            upward.x = _blade.normal.y;
            upward.y = -_blade.normal.x;
            upward.z = _blade.normal.z;
            Vector3 left = Vector3.Cross(_blade.normal, upward);

            Vector3 displacement = Vector3.zero;
            Vector2 newUV1 = Vector2.zero;
            Vector2 newUV2 = Vector2.zero;
            Vector2 newUV3 = Vector2.zero;

            int iterator = 0;
            while (indices.Count > 2)
            {

                Vector3 link1 = _newVerticesCache[indices[iterator]];
                Vector3 link2 = _newVerticesCache[indices[(iterator + 1) % indices.Count]];
                Vector3 link3 = _newVerticesCache[indices[(iterator + 2) % indices.Count]];

                displacement = link1 - center;
                newUV1 = Vector3.zero;
                newUV1.x = 0.5f + Vector3.Dot(displacement, left);
                newUV1.y = 0.5f + Vector3.Dot(displacement, upward);

                displacement = link2 - center;
                newUV2 = Vector3.zero;
                newUV2.x = 0.5f + Vector3.Dot(displacement, left);
                newUV2.y = 0.5f + Vector3.Dot(displacement, upward);

                displacement = link3 - center;
                newUV3 = Vector3.zero;
                newUV3.x = 0.5f + Vector3.Dot(displacement, left);
                newUV3.y = 0.5f + Vector3.Dot(displacement, upward);


                _newTriangleCache.vertices[0] = link1;
                _newTriangleCache.uvs[0] = newUV1;
                _newTriangleCache.normals[0] = -_blade.normal;
                _newTriangleCache.tg[0] = Vector4.zero;

                _newTriangleCache.vertices[1] = link2;
                _newTriangleCache.uvs[1] = newUV2;
                _newTriangleCache.normals[1] = -_blade.normal;
                _newTriangleCache.tg[1] = Vector4.zero;

                _newTriangleCache.vertices[2] = link3;
                _newTriangleCache.uvs[2] = newUV3;
                _newTriangleCache.normals[2] = -_blade.normal;
                _newTriangleCache.tg[2] = Vector4.zero;

                NormalCheck(ref _newTriangleCache);

                _leftSide.AddTriangle(_newTriangleCache, _capMatSub);

                _newTriangleCache.normals[0] = _blade.normal;
                _newTriangleCache.normals[1] = _blade.normal;
                _newTriangleCache.normals[2] = _blade.normal;

                NormalCheck(ref _newTriangleCache);

                _rightSide.AddTriangle(_newTriangleCache, _capMatSub);

                indices.RemoveAt((iterator + 1) % indices.Count);

                iterator = (iterator + 1) % indices.Count;
            }

		}
        private static void FillCap_Method2(List<int> indices)
        {
            Vector3 center = Vector3.zero;
            foreach (var index in indices)
                center += _newVerticesCache[index];

            center = center / indices.Count;

            Vector3 upward = Vector3.zero;
            upward.x = _blade.normal.y;
            upward.y = -_blade.normal.x;
            upward.z = _blade.normal.z;
            Vector3 left = Vector3.Cross(_blade.normal, upward);

            Vector3 displacement = Vector3.zero;
            Vector2 newUV1 = Vector2.zero;
            Vector2 newUV2 = Vector2.zero;

            for (int i = 0; i < indices.Count - 1; i++)
            {

                displacement = _newVerticesCache[indices[i]] - center;
                newUV1 = Vector3.zero;
                newUV1.x = 0.5f + Vector3.Dot(displacement, left);
                newUV1.y = 0.5f + Vector3.Dot(displacement, upward);

                displacement = _newVerticesCache[indices[i + 1]] - center;
                newUV2 = Vector3.zero;
                newUV2.x = 0.5f + Vector3.Dot(displacement, left);
                newUV2.y = 0.5f + Vector3.Dot(displacement, upward);



                _newTriangleCache.vertices[0] = _newVerticesCache[indices[i]];
                _newTriangleCache.uvs[0] = newUV1;
                _newTriangleCache.normals[0] = -_blade.normal;
                _newTriangleCache.tg[0] = Vector4.zero;

                _newTriangleCache.vertices[1] = _newVerticesCache[indices[i + 1]];
                _newTriangleCache.uvs[1] = newUV2;
                _newTriangleCache.normals[1] = -_blade.normal;
                _newTriangleCache.tg[1] = Vector4.zero;

                _newTriangleCache.vertices[2] = center;
                _newTriangleCache.uvs[2] = new Vector2(0.5f, 0.5f);
                _newTriangleCache.normals[2] = -_blade.normal;
                _newTriangleCache.tg[2] = Vector4.zero;


                NormalCheck(ref _newTriangleCache);

                _leftSide.AddTriangle(_newTriangleCache, _capMatSub);

                _newTriangleCache.normals[0] = _blade.normal;
                _newTriangleCache.normals[1] = _blade.normal;
                _newTriangleCache.normals[2] = _blade.normal;

                NormalCheck(ref _newTriangleCache);

                _rightSide.AddTriangle(_newTriangleCache, _capMatSub);

            }

        }
        #endregion

        #region Misc.
        private static void NormalCheck(ref MeshMaker.Triangle triangle)
        {
            Vector3 crossProduct = Vector3.Cross(triangle.vertices[1] - triangle.vertices[0], triangle.vertices[2] - triangle.vertices[0]);
            Vector3 averageNormal = (triangle.normals[0] + triangle.normals[1] + triangle.normals[2]) / 3.0f;
            float dotProduct = Vector3.Dot(averageNormal, crossProduct);
            if (dotProduct < 0)
            {
                Vector3 temp = triangle.vertices[2];
                triangle.vertices[2] = triangle.vertices[0];
                triangle.vertices[0] = temp;

                temp = triangle.normals[2];
                triangle.normals[2] = triangle.normals[0];
                triangle.normals[0] = temp;

                Vector2 temp2 = triangle.uvs[2];
                triangle.uvs[2] = triangle.uvs[0];
                triangle.uvs[0] = temp2;
                
                Vector4 temp3 = triangle.tg[2];
                triangle.tg[2] = triangle.tg[0];
                triangle.tg[0] = temp3;
            }

        }
        #endregion
    }
}