﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
namespace AXIOM_WIZARD
{
    public static class SaveSys
    {
        public static void SaveProp(Props prop)
        {
            BinaryFormatter form = new BinaryFormatter();
            string path = Application.persistentDataPath + "/axiom_wizard.aw";
            FileStream stream = new FileStream(path, FileMode.Create);
            PropsData data = new PropsData(prop);
            form.Serialize(stream, data);
            stream.Close();
        }

        public static PropsData LoadProp()
        {
            string path = Application.persistentDataPath + "/axiom_wizard.aw";
            if (File.Exists(path))
            {
                BinaryFormatter form = new BinaryFormatter();
                FileStream stream = new FileStream(path, FileMode.Open);

                PropsData data = form.Deserialize(stream) as PropsData;
                stream.Close();
                return data;
            }
            else
            {
                Debug.LogError("This file not found in " + path);
                return null;
            }
        }
    }
}