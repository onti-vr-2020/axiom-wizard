﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AXIOM_WIZARD
{

    [System.Serializable]
    public class PropsData
    {
        public float[] position;
        public PropsData(Props prop)
        {
            position = new float[3];
            position = prop.GetPosition();
        }
    }

}