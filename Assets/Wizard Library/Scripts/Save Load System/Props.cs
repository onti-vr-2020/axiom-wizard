﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AXIOM_WIZARD;

public class Props : MonoBehaviour
{
    public void SaveProp()
    {
        SaveSys.SaveProp(this);
    }

    public void LoadProp()
    {
        PropsData data = SaveSys.LoadProp();
        Vector3 position;
        position.x = data.position[0];
        position.y = data.position[1];
        position.z = data.position[2];
        this.transform.position = position;

    }

    public float[] GetPosition()
    {
        float[] pos;
        pos = new float[3];
        pos[0] = this.transform.position.x;
        pos[1] = this.transform.position.y;
        pos[2] = this.transform.position.z;
        return pos;
    }
}
