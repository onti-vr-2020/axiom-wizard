﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AXIOM_WIZARD;
using Valve.VR.InteractionSystem;
public class ToolUser : MonoBehaviour
{
    Material[] capMaterialArray;
    public Material CapMaterial;
    public GameObject[] Obj;
    public GameObject ToCrop;
    int pieceCounter = 0;
    void Start()
    {
        capMaterialArray = GameObject.Find("MaterialList").GetComponent<MaterialList>().Materials;
    }
    void Update()
    {
        if (Input.GetMouseButtonDown(5))
        {
            GameObject resultMesh = new GameObject();
            resultMesh.name = "Combined Figure";
            resultMesh.tag = "Obj";
            resultMesh.AddComponent<MeshFilter>();
            resultMesh.AddComponent<MeshRenderer>();
            resultMesh.AddComponent<MeshCombiner>().Combine(CapMaterial, Obj);
        }
        if (Input.GetMouseButtonDown(3))
        {
            ToCrop.GetComponent<MeshCombiner>().Slice(ToCrop);
        }
    }
    void OnDrawGizmosSelected()
    {

        Gizmos.color = Color.green;

        Gizmos.DrawLine(transform.position, transform.position + transform.forward * 5.0f);
        Gizmos.DrawLine(transform.position + transform.up * 0.5f, transform.position + transform.up * 0.5f + transform.forward * 5.0f);
        Gizmos.DrawLine(transform.position + -transform.up * 0.5f, transform.position + -transform.up * 0.5f + transform.forward * 5.0f);

        Gizmos.DrawLine(transform.position, transform.position + transform.up * 0.5f);
        Gizmos.DrawLine(transform.position, transform.position + -transform.up * 0.5f);

    }
    public void Slice(string Tag)
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, transform.forward, out hit))
        {
            if (Tag == "stone")
            {
                Tag = hit.collider.tag;
            }
            if (hit.collider.tag == Tag)
            {
                if (Tag == "LogOne")
                {
                    CapMaterial = capMaterialArray[0];
                }
                else if (Tag == "LogTwo")
                {
                    CapMaterial = capMaterialArray[1];
                }
                else if (Tag == "WoodOne")
                {
                    CapMaterial = capMaterialArray[2];
                }
                else if (Tag == "WoodTwo")
                {
                    CapMaterial = capMaterialArray[3];
                }
                else if (Tag == "Cobblestone")
                {
                    CapMaterial = capMaterialArray[4];
                }
                else if (Tag == "Stone")
                {
                    CapMaterial = capMaterialArray[5];
                }
                else if (Tag == "IronListNew")
                {
                    CapMaterial = capMaterialArray[6];
                }
                else if (Tag == "IronListOld")
                {
                    CapMaterial = capMaterialArray[7];
                }
                GameObject[] pieces = MeshCut.Cut(hit.collider.gameObject, transform.position, transform.right, CapMaterial);

                for (int i = 0; i <= 1; i++)
                {
                    if (!pieces[i].GetComponent<MeshCollider>())
                    {
                        pieces[i].AddComponent<MeshCollider>();
                    }
                    else
                    {
                        Destroy(pieces[i].GetComponent<MeshCollider>());
                        pieces[i].AddComponent<MeshCollider>().convex = true;
                    }
                    pieces[i].GetComponent<MeshCollider>().convex = true;
                    if (!pieces[i].GetComponent<Rigidbody>())
                    {
                        pieces[i].AddComponent<Rigidbody>();
                    }
                    pieces[i].transform.parent = null;
                    pieces[i].AddComponent<Interactable>();
                    pieces[i].AddComponent<Throwable>();
                    pieces[i].AddComponent<VelocityEstimator>();
                    pieces[i].AddComponent<GravitationControll>();
                }

                if (GameObject.Find("left side").transform.childCount != 0)
                {
                    int count = GameObject.Find("left side").transform.childCount;
                    for (int i = 0; i < count; i++)
                    {
                        Destroy(GameObject.Find("left side").transform.GetChild(i).gameObject);
                    }
                    
                }
                GameObject.Find("left side").tag = Tag;

                GameObject.Find("left side").name += " " + System.Convert.ToString(pieceCounter++);

                if (GameObject.Find("right side").transform.childCount != 0)
                {
                    int count = GameObject.Find("right side").transform.childCount;
                    for (int i = 0; i < count; i++)
                    {
                        Destroy(GameObject.Find("right side").transform.GetChild(i).gameObject);
                    }
                }
                GameObject.Find("right side").GetComponent<Rigidbody>().AddForce(transform.up * 100.0f);
                GameObject.Find("right side").tag = Tag;
                GameObject.Find("right side").name += " " + System.Convert.ToString(pieceCounter++);
            }
        }
    }
}