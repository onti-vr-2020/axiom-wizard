﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class Hand : MonoBehaviour
{

    public SteamVR_Input_Sources myaur;
    public SteamVR_Action_Boolean f;

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "Hand") {

            if (f.GetStateDown(myaur) || Input.GetKeyDown(KeyCode.R))
            {
                this.GetComponent<Rigidbody>().isKinematic = !this.GetComponent<Rigidbody>().isKinematic;
            }
        }
    }
}
