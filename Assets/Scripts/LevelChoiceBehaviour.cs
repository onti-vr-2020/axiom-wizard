﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelChoiceBehaviour : MonoBehaviour
{
    public Image image;
    public Text text;
   public void CheckLevel(Button btn)
    {
        string name = btn.name;
        string path = "PreviewShots/" + name;
        image.sprite = Resources.Load<Sprite>(path);

        /*path = "Assets/Resources/PreviewTexts/" + name + ".txt";
        StreamReader read = new StreamReader(path);
        string content = read.ReadToEnd();
        text.text = content;*/

        
    }

    public void StartScene()
    {
        string name = image.sprite.name.ToString();
        Application.LoadLevel(name);

    }
}
