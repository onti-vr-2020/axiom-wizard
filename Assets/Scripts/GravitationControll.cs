﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;
using System;

public class GravitationControll : MonoBehaviour
{
    public bool Status = true;
    bool st, st1;
    string stTag;
    GameObject obj;
    private void Start()
    {
        stTag = this.tag;
    }
    public void Gravitation(bool state)
    {
        Status = !Status;
    }

    void FixedUpdate()
    {
        if (!Status)
        {
            this.GetComponent<Rigidbody>().isKinematic = true;
            this.tag = "Hover";
        }

    }
}