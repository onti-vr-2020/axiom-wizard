﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class ScissorsSlicer : MonoBehaviour
{
    public Animator LeftCut;
    public SteamVR_Input_Sources myaur;
    public SteamVR_Action_Boolean f;

    void OnTriggerStay(Collider other)
    {
        if (other.tag == "IronListNew" || other.tag == "IronListOld")
        {
            LeftCut.GetComponent<Animator>().Play("LeftScissors");
        }
        if (f.GetStateDown(myaur))
        {
            GameObject.Find("scissorsBlade").GetComponent<ToolUser>().Slice(other.tag);
        }
    }
    
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "IronListNew" || other.tag == "IronListOld")
        {
            LeftCut.GetComponent<Animator>().Play("New State");
        }
    }
}
