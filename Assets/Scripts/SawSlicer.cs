﻿using System.Collections; 
using System.Collections.Generic;
using UnityEngine;

public class SawSlicer : MonoBehaviour
{
    int counter = 0;
    Vector3 startPosition;
    bool changeState = false;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LogOne" || other.tag == "LogTwo" || other.tag == "WoodOne" || other.tag == "WoodTwo")
        {
            startPosition = this.transform.InverseTransformDirection(this.transform.position);
        }
    }
    void OnTriggerStay(Collider other)
    {

        if (other.tag == "LogOne" || other.tag == "LogTwo" || other.tag == "WoodOne" || other.tag == "WoodTwo")
        {
            if (counter > 5)
            {
                this.transform.GetChild(0).GetComponent<ToolUser>().Slice(other.tag);
                counter = 0;
            }
            if (Mathf.Sqrt(Mathf.Pow((startPosition.x - this.transform.InverseTransformDirection(this.transform.position).x), 2) + Mathf.Pow((startPosition.y - this.transform.InverseTransformDirection(this.transform.position).y), 2) + Mathf.Pow((startPosition.z - this.transform.InverseTransformDirection(this.transform.position).z), 2)) > 0.7f)
            {
                counter++;
                Debug.Log(counter);
                startPosition = this.transform.InverseTransformDirection(this.transform.position);
            }
            
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "LogOne" || other.tag == "LogTwo" || other.tag == "WoodOne" || other.tag == "WoodTwo")
        {
            counter = 0;
        }
    }
}
