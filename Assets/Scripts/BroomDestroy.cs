﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BroomDestroy : MonoBehaviour
{
    int count = 0;
    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "LogOne" || other.tag == "LogTwo" || other.tag == "WoodOne" || other.tag == "WoodTwo" || other.tag == "Cobblestone" || other.tag == "Stone" || other.tag == "IronListNew" || other.tag == "IronListOld")
        {
            if  (count > 3)
            {
                Destroy(other.gameObject);
                count = 0;
            }
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.tag == "LogOne" || other.tag == "LogTwo" || other.tag == "WoodOne" || other.tag == "WoodTwo" || other.tag == "Cobblestone" || other.tag == "Stone" || other.tag == "IronListNew" || other.tag == "IronListOld")
        {
            count++;
        }
    }
}
