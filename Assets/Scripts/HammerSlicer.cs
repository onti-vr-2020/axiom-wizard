﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HammerSlicer : MonoBehaviour
{

    int count = 0;
    System.Random rnd = new System.Random();
    int max;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "HammerTool")
        {
            if (count == 0)
            {
                max = rnd.Next(3, 6);
            }
            if (count == max)
            {
                this.transform.GetChild(0).GetComponent<ToolUser>().Slice("stone");
                count = 0;
            }
            else
            {
                count++;
            }
        }
    }
}
