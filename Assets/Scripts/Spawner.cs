﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public string TypeTag;
    public Vector3 SpawnPosition;
    public Vector3 Angle;
    public GameObject Material;
    public int counter = 0;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == TypeTag)
        {
            counter++;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        
        if (other.tag == TypeTag)
        {
            if (counter <= 1)
            {
                Instantiate(Material, SpawnPosition, Quaternion.Euler(Angle));
            }
            counter--;
        }
    }
}
