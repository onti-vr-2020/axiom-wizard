﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;

public class SwitchGravity : MonoBehaviour
{
    public SteamVR_Input_Sources myaur;
    public SteamVR_Action_Boolean f;

    private void OnTriggerStay(Collider other)
    {
        if (other.tag == "LogOne" || other.tag == "LogTwo" || other.tag == "WoodOne" || other.tag == "WoodTwo" || other.tag == "Cobblestone" || other.tag == "Stone" || other.tag == "IronListNew" || other.tag == "IronListOld" || other.tag == "Hover")
        {
            if (f.GetStateDown(myaur))
            {
                other.gameObject.GetComponent<GravitationControll>().Gravitation(!other.gameObject.GetComponent<GravitationControll>().Status);
            }
        }
    }
}