﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using AXIOM_WIZARD;

public class CombineAll : MonoBehaviour
{
    public SteamVR_Input_Sources myaur;
    public SteamVR_Action_Boolean f;
    private void Update()
    {
        if (f.GetStateDown(myaur))
        {
            GameObject[] obj;
            obj = GameObject.FindGameObjectsWithTag("Hover");
            GameObject resultMesh = new GameObject();
            resultMesh.name = "Combined Figure";
            resultMesh.tag = "Result";
            resultMesh.AddComponent<MeshFilter>();
            resultMesh.AddComponent<MeshRenderer>();
            resultMesh.AddComponent<MeshCombiner>().Combine(null, obj);
        }
    }
}
